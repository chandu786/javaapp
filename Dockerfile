FROM maven:3.5-jdk-8-alpine
WORKDIR /app     
RUN apk add git
RUN git clone https://gitlab.com/ot-interview/javaapp.git /app
RUN mvn package
FROM tomcat:8.0
WORKDIR /app
COPY --from=0 /app/target/Spring3HibernateApp.war /usr/local/tomcat/webapps      
EXPOSE 8080




##In this file we are using maven for our java application. We will directly clone our code from gitlab to /app were we will run the our package and with  this it will generate war file which we will copy in our tomcat webserver . As tomcat bydefault use 8080 port number so we will expose this port in our docker file. 


#Commands to run docker file

#sudo docker build -t javaassignment .   >> With This command docker file will execute and create an image
#sudo docker image                       >> With this command we can check the image which we have created 
#sudo docker run -d --name javaprojectassignment -p 9090:8080 javaassignment >> With this command we will create and run our docker container in background and map port on 9090 and use our image which we have created recently with our docker file.

#Once our container is created we will use #sudo docker ps command to check the status of container. 

# To check whether our java application is running or not we will go to browser and check on port 9090 
# http://<ip_address>:9090/Spring3HibernateApp 

